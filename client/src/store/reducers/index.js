import { combineReducers } from "@reduxjs/toolkit";
import commentsReducer from "./commentsReducer"
import postsReducer from "./postReducer"

const reducer = combineReducers({
  comments:commentsReducer,
  posts:postsReducer
  
});

export default reducer;
