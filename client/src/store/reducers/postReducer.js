import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const initialState = {
  posts: [],
  onepost:{},
};

export const loadPosts = createAsyncThunk(
  "posts/loadPost",
  async (_, thunkAPI) => {
    try {
      console.log("post");
      const { data } = await axios.get(
        "https://jsonplaceholder.typicode.com/posts"
      );
      return data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
export const addPost = createAsyncThunk(
  "posts/addPost",
  async (raw, thunkAPI) => {
    try {
      console.log("post");
      const { data } = await axios.post(
        "https://jsonplaceholder.typicode.com/posts",
        {
          title: raw.title,
          userId: raw.id,
          body: raw.body,
        }
      );
      console.log(data);
      return data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const deletePost = createAsyncThunk(
  "posts/deletePost",
  async (raw, thunkAPI) => {
    try {
      console.log("post");
       await axios.delete(
        `https://jsonplaceholder.typicode.com/posts/${raw.id}`
      );
      console.log(raw);
  
      return raw;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    onePost: (state, action) => {
      const o = state.posts.find(post=>{
        return post.id == action.payload.id
      })
      state.onepost = o
      //   logic
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadPosts.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loadPosts.fulfilled, (state, action) => {
        state.isLoading = false;
        state.posts = action.payload;
      })
      .addCase(loadPosts.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
    builder
      .addCase(addPost.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(addPost.fulfilled, (state, action) => {
        state.isLoading = false;
        state.posts.push(action.payload);
      })
      .addCase(addPost.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
    builder
      .addCase(deletePost.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deletePost.fulfilled, (state, action) => {
        state.isLoading = false;
        var index = state.posts.findIndex((post) => {
          return action.payload.id == post.id;
        });
        state.posts.splice(index, 1);
      })
      .addCase(deletePost.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
  },
});

export const { onePost } = postsSlice.actions;

export default postsSlice.reducer;
