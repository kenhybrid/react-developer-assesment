import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";


const initialState = {
  comments: [],
};

export const loadComments = createAsyncThunk(
  "comments/loadComments",
  async (_, thunkAPI) => {
   try {
    console.log("comments")
    const { data } = await axios.get(
        "https://jsonplaceholder.typicode.com/comments"
      );
    return data
   } catch (error) {
    return thunkAPI.rejectWithValue(error);
   }
});

export const commentsSlice = createSlice({
  name: "comments",
  initialState,
  reducers: {
    like: (state, action) => {
    //   logic
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadComments.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loadComments.fulfilled, (state, action) => {
        state.isLoading = false;
        state.comments = action.payload;
      })
      .addCase(loadComments.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
       
      });
   
  },
});

export const { all } = commentsSlice.actions;

export default commentsSlice.reducer;
