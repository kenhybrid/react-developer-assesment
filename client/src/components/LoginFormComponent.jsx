import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext, useAuth } from "../hooks/useAuth";
export default function LoginFormComponent() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { signIn,error } = useAuth();
  let navigate = useNavigate();

  const logInUser = async (e) => {
    e.preventDefault();
    // login oop
    try {
      await signIn({ email, password });
    } catch (error) {
      console.log(error);
    }

    // redirect user
  };
  return (
    <div className="flex justify-center h-screen items-center">
      <div className="p-4">
        <div className="text-center text-5xl font-bold mb-4">Login</div>
        <div className="mb-6">use an email (from the json api) and a random password to login</div>
    {
      error ?     <div className="p-3 bg-red-500 text-white">{error}</div>:<></>
    }
        <form onSubmit={(e) => logInUser(e)}>
          <div className="mb-2">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              required
              onChange={(e) => setEmail(e.target.value)}
              className="w-full border-black p-3 border-2 outline-none rounded-sm mt-2"
            />
            <p> hint: example email <b>Sincere@april.biz</b></p>
          </div>
          <div className="mb-2">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              required
              onChange={(e) => setPassword(e.target.value)}
              className="w-full border-black p-3 border-2 outline-none rounded-sm mt-2"
            />
          </div>
          <div className="mb-2">
            <button className="w-full bg-black p-3 text-white rounded-sm">
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
