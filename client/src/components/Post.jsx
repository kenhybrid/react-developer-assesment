import React, { useState } from "react";
import {
  EyeIcon,
  HeartIcon,
  PencilIcon,
  TrashIcon,
} from "@heroicons/react/outline";
import { HeartIcon as Heart2 } from "@heroicons/react/solid";
import { useDispatch } from "react-redux";
import { deletePost } from "../store/reducers/postReducer";
import { Link } from "react-router-dom";

export default function Post({ post }) {
  return (
    <>
      <div className="flex justify-between items-center p-4 rounded-sm bg-gray-100 mb-1.5">
        <div>
          <div className="font-semibold text-lg capitalize">
            {post.id}. {post.title}
          </div>

          <div className="text-sm">{post.body}.</div>
        </div>
        <div className="flex justify-evenly space-x-4">
          <Link to={"/post/" + post.id}>
            <EyeIcon className="h-5 w-5 text-red-500" />
          </Link>
        </div>
      </div>
    </>
  );
}
