import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useAuth } from "../hooks/useAuth";
import { addPost, } from "../store/reducers/postReducer";

export default function AddPostComponent() {
  // const {user:{id}}= useAuth()
  const id = 4;
  const [body, setBody] = useState("");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch()
  const addPostData = (e) => {
    e.preventDefault();
    
    dispatch(addPost({title,body,id}))
  };
  return (
    <>
      <form onSubmit={(e) => addPostData(e)} className=" p-2">
        <div className="mb-2">
          <label htmlFor="title">Title</label>
          <input type="text" required className=" border p-2 border-gray-700 w-full rounded-md" 
          onChange={e=>setTitle(e.target.value)}
          value={title}
          />
        </div>
        <label htmlFor="PostMessage">Post Body</label>

        <div className="mt-3">
          <textarea
           onChange={e=>setBody(e.target.value)}
           value={body}
           required
            id="PostMessage"
            rows="4"
            className=" border p-2 border-gray-700 w-full rounded-md"
           
          ></textarea>
        </div>
        <button className="w-full bg-black p-3 text-white rounded-md">
          Add Post
        </button>
      </form>
    </>
  );
}
