import React, { useContext } from "react";
import { Link, useNavigate,unstable_HistoryRouter } from "react-router-dom";
import { AuthContext, useAuth } from "../hooks/useAuth";

export default function NavigationBar() {
    const { user } = useAuth()
  let navigate = useNavigate();
  const logOut = () => {
    localStorage.removeItem("user")
    
    navigate("/login");
    window.location.reload(false);
  };
  return (
    <nav className="p-2 md:px-24 px-6 bg-black text-white fixed flex justify-between items-center w-full">
      <div>App</div>
      <div className="flex justify-evenly items-center space-x-4">
      
        {user && user ? (
         <>
           <Link to="/">Home</Link>
        <Link to="profile">Profile</Link>
        <Link to="posts">Posts</Link></>
        ) : (
          <></>
        )} 
        {user && user ? (
          <button onClick={logOut} className="bg-red-400 p-2 rounded-md">
            Logout
          </button>
        ) : (
          <></>
        )} 
      </div>
    </nav>
  );
}
