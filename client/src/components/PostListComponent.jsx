import React from "react";
import Post from "./Post";

export default function PostListComponent({ posts }) {
  return (
    <div className="p-2">
      {posts &&
        posts.map((post) => {
          return <Post key={post.id} post={post} />;
        })}
    </div>
  );
}
