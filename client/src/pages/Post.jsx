import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";
import { deletePost } from "../store/reducers/postReducer";

export default function Post() {
  const{user}= useAuth()
  const { id } = useParams();
  const [modal, setModal] = useState(false);
  const [comment, setComment] = useState("");
  const [comments, setComments] = useState([]);
  const [liked, setLiked] = useState(true);
  const [post, setPost] = useState({});

  useEffect(() => {
    async function loadPost() {
      try {
        console.log("post");
        const { data } = await axios.get(
          `https://jsonplaceholder.typicode.com/posts/${id}`
        );
        console.log(data);
        setPost(data);
        return data;
      } catch (error) {
        console.log(error);
      }
    }
    async function loadComments() {
      try {
        console.log("post");
        const { data } = await axios.get(
          `https://jsonplaceholder.typicode.com/posts/${id}/comments`
        );
        console.log(data);
        setComments(data);
        return data;
      } catch (error) {
        console.log(error);
      }
    }
    loadComments();
    loadPost();
  }, [id]);

  const toggleModal = () => {
    setModal(!modal);
    console.log(modal);
  };
  const dispatch = useDispatch();
  const nav = useNavigate();
  const deleteThisPost = () => {
    dispatch(deletePost({ id }));
    nav("/posts");
    setModal(false);
  };

  const commentPost = async (e) => {
    e.preventDefault();
    try {
      console.log("post");
      const { data } = await axios.post(
        `https://jsonplaceholder.typicode.com/comments`,
        {
          userId: user.id,
          body: comment,
          email:user.email,
          name:user.name,
        }
      );
    
      setComments(oldArray => [...oldArray, data]);
      return data;
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="pt-16 md:px-20 px-2">
        <button className="bg-red-400 p-2 rounded-lg" onClick={toggleModal}>
          Delete Post
        </button>

        <div className="my-2 mt-10 font-bold text-xl">
          {id} .{post.title}
        </div>
        <div className="py-3">{post.body}</div>

        <form onSubmit={(e) => commentPost(e)}>
          <div className="mt-3">
            <textarea
              onChange={(e) => setComment(e.target.value)}
              value={comment}
              required
              id="PostMessage"
              rows="4"
              className=" border p-2 border-gray-700 w-full rounded-md"
            ></textarea>
          </div>
          <button className="w-full bg-black p-3 text-white rounded-md">
            Add comment
          </button>
        </form>

        <div className="p-3">
          {comments &&
            comments.map((c) => {
              return (
                <div key={c.id} className=" border p-2 m-1">
                  <div className="text-md">{c.body}</div>
                  <div className="font-bold text-sm">
                    BY: -- {c.name} | {c.email}
                  </div>
                </div>
              );
            })}
        </div>
      </div>

      <div
        className={
          modal == true
            ? "bg-black bg-opacity-30 flex z-50 w-full absolute inset-0  justify-center items-center "
            : "hidden"
        }
        // onClick={() => toggleModal()}
      >
        <div className="shadow-sm max-w-2xl w-full">
          <div className="relative px-4 w-100 min-w-full min-w-3xl h-full md:h-auto">
            <div className="relative bg-white rounded-lg shadow p-10">
              <div className="text-center">
                Are you sure you want to delete this post?
              </div>
              <div className="flex justify-between">
                <button
                  onClick={toggleModal}
                  className="bg-green-500 p-3 rounded-lg"
                >
                  No
                </button>
                <button
                  onClick={deleteThisPost}
                  className="bg-red-500 p-3 rounded-lg"
                >
                  Yes
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
