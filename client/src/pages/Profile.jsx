import React, { useContext, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { AuthContext, useAuth } from "../hooks/useAuth";
import axios from "axios";
export default function Profile() {
  const { user, isLoading } = useAuth();
  const [name, setName] = useState("");

  const [profile, setUser] = useState({
    username:"",
    email:"",
    website:"",
    company:{
      bs:"",
      catchPhrase:"",
      name:"",
    },
    address:{
      street:"",
      suite:"",
      zipcode:"",
      city:"",
    }
  });
  const id = 3
  useEffect(() => {
    console.log(user.id)
    async function load() {
      try {
        console.log("post");
        const { data } = await axios.get(
          `https://jsonplaceholder.typicode.com/users/${user.id}`
        );
        console.log(data);
        setUser(data);
        return data;
      } catch (error) {
        console.log(error);
      }
    }
    load();
  }, [id]);
  if (user) {
    return (
      <div className="pt-20 md:px-20 px-4 min-h-screen">
        <form action="">
          <div className="my-3">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="w-full p-3 border-2 mt-1"
              value={profile.name}
            />
          </div>
          <hr />
          <div className="grid md:grid-cols-3 my-4 grid-cols-1 gap-4">
            <div className="mb-2">
              <label htmlFor="name">Username</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.username}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="email">Email</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.email}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Phone</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.phone}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Website</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.website}
              />
            </div>
          </div>
          <div className="font-bold mb-1">Company:</div>
          <hr />
          <div className="grid md:grid-cols-3 my-4 grid-cols-1 gap-4">
            <div className="mb-2">
              <label htmlFor="name">Bs</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.company.bs}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Catchphrase</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.company.catchPhrase}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.company.name}
              />
            </div>
          </div>
          <div className="font-bold mb-1">Address:</div>
          <hr />
          <div className="grid md:grid-cols-4 my-4 grid-cols-1 gap-4">
            <div className="mb-2">
              <label htmlFor="name">City</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.address.city}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Street</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.address.street}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Suit</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                onChange={(e) => setUser(e.target.value)}
                value={profile.address.suite}
              />
            </div>
            <div className="mb-2">
              <label htmlFor="name">Zipcode</label>
              <input
                type="text"
                className="w-full p-3 border-2 mt-1"
                value={profile.address.zipcode}
              />
            </div>
          </div>
          <div className="my-3">
            <button className="bg-black p-3 rounded-md text-white w-full">
              Update user
            </button>
          </div>
        </form>
      </div>
    );
  }
  return (
    <>
      <Navigate to="/login"></Navigate>
    </>
  );
}
