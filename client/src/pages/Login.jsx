import React,{Suspense,lazy} from "react";

const LoginFormComponent = lazy(() =>
  import("../components/LoginFormComponent")
);

export default function Login() {
  return (
    <div className="pt-16 px-20">
      <Suspense fallback={<div>Loading...</div>}>
        <LoginFormComponent />
      </Suspense>
    </div>
  );
}
