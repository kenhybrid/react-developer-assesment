import axios from "axios";
import React, { useEffect, useState, Suspense, lazy, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import Loader from "../components/Loader";
import { AuthContext, useAuth } from "../hooks/useAuth";
import { loadComments } from "../store/reducers/commentsReducer";
import { loadPosts } from "../store/reducers/postReducer";

const PostListComponent = lazy(() => import("../components/PostListComponent"));
const AddPostComponent = lazy(() => import("../components/AddPostComponent"));

export default function Posts() {
  const dispatch = useDispatch();

  const { user, isLoading } = useAuth();
  useEffect(() => {
    dispatch(loadPosts());
    dispatch(loadComments());
  }, []);
  const [add, setAdd] = useState(true);
  const { posts } = useSelector((state) => state.posts);

  const toggle = () => {
    setAdd(!add);
  };
  if (user) {
    return (
      <div className="pt-16 md:px-20 px-2">
        <button
          className="mx-2 p-3 bg-black text-white rounded-md"
          onClick={toggle}
        >
          Toggle Add Post {add}
        </button>
        {add == false ? (
          <Suspense fallback={<Loader />}>
            <AddPostComponent />
          </Suspense>
        ) : (
          <></>
        )}

        <Suspense fallback={<div>Loading...</div>}>
          <PostListComponent posts={posts} />
        </Suspense>


      </div>
    );
  }

  return (
    <>
      <Navigate to="/login"></Navigate>
    </>
  );
}
