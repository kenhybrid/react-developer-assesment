import { useContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Link, Navigate } from "react-router-dom";
import { AuthContext, useAuth } from "../hooks/useAuth";
import { loadComments } from "../store/reducers/commentsReducer";
import { loadPosts } from "../store/reducers/postReducer";

function Home() {
  // const { user, isLoading } = useContext(AuthContext);
  const { user, isLoading } = useAuth();


  if (user) {
    return (
      <>
        <div className="pt-16 px-20">
          <h1 className="mt-4  text-3xl">Welcome back ,<span className="font-semibold">{user.name}</span> </h1>
          <h1>This is the home page</h1>
          
        </div>
      </>
    );
  }
  return (
    <>
      <Navigate to="/login"></Navigate>
    </>
  );
}

export default Home;
