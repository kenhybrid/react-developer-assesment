import React, { useContext, lazy, Suspense } from "react";
import { Route } from "react-router-dom";
import Loader from "../components/Loader";
import { AuthContext, useAuth } from "./useAuth";
const Login = lazy(() => import("../pages/Login"));

export default function ProtectedRoute({ path, element }) {
  const auth = useContext(AuthContext);
  if (isLoading) {
    return <Loader />;
  }
  if (user) {
    return <>
    <Route path={path} element={element} /></>;
  }
  //redirect if there is no user
  return (
   <>
    <Route
      path="login"
      element={
        <Suspense fallback={<>...</>}>
          <Login />
        </Suspense>
      }
    /></>
  );
}
