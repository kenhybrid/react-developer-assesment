import axios from "axios";
import { createContext, useContext, useEffect, useState } from "react";
import { unstable_HistoryRouter, useNavigate } from "react-router-dom";

export const AuthContext = createContext();

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  const auth = useAuthProvider();
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};

const useAuthProvider = () => {
  // userFromLocalStorage
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("user")) | {}
  );
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  let navigateTo = useNavigate();
  useEffect(() => {
    const u = JSON.parse(localStorage.getItem("user"));
    if (u) {
      setUser(u);
    }
  }, []);

  const signIn = async ({ email, password }) => {
    try {
      // login code from api
      const { data } = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );

      const match = data.filter((m) => {
        return m.email == email;
      });
      console.log(match[0]);
      if (match[0]) {
        setUser(match[0]);

        localStorage.setItem("user", JSON.stringify(match[0]));
        navigateTo("/");
      }
      setError("You are not registered")
      // setUser(user);
      // setUser({email,password});

      // localStorage.setItem("user",JSON.stringify({email,password}))

      // // after successful login
      // navigateTo("/");
    } catch (error) {
      console.log(error);
      setError(error.message);
    }
  };
  const getUser = () => {
    if (user == {}) {
      setUser(JSON.parse(localStorage.getItem("user")) | {});
      navigateTo("/");
    }
  };

  return {
    user,
    loading,
    signIn,

    error,
    getUser,
  };
};
