import { Routes, Route } from "react-router-dom";
import NavigationBar from "./components/NavigationBar";
import React, { lazy } from "react";
import { AuthProvider } from "./hooks/useAuth";
import { Provider } from "react-redux";
import { store } from "./store/store";

const Home = lazy(() => import("./pages/Home"));
const Login = lazy(() => import("./pages/Login"));
const Profile = lazy(() => import("./pages/Profile"));
const Posts = lazy(() => import("./pages/Posts"));
const Post = lazy(() => import("./pages/Post"));

export default function App() {
  return (
    <>
      <AuthProvider>
        <Provider store={store}>
          <NavigationBar />

          <Routes>
         
            <Route
              path="/"
              element={
                <React.Suspense fallback={<>...</>}>
                  <Home />
                </React.Suspense>
              }
            />
            <Route
              path="login"
              element={
                <React.Suspense fallback={<>...</>}>
                  <Login />
                </React.Suspense>
              }
            />
            <Route
              path="post/:id"
              element={
                <React.Suspense fallback={<>...</>}>
                  <Post />
                </React.Suspense>
              }
            />
            <Route
              path="profile"
              element={
                <React.Suspense fallback={<>...</>}>
                  <Profile />
                </React.Suspense>
              }
            />
            <Route
              path="posts"
              element={
                <React.Suspense fallback={<>...</>}>
                  <Posts />
                </React.Suspense>
              }
            />
          </Routes>
        </Provider>
      </AuthProvider>
    </>
  );
}
